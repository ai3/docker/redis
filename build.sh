#!/bin/sh
#
# Install script for Redis inside a Docker container.
#

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

die() {
    echo "ERROR: $*" >&2
    exit 1
}

set -x

apt update

# Install Redis.
install_packages redis-server \
    || die "could not install packages"

# Create mountpoint dirs.
mkdir -p /etc/redis
mkdir -p /var/lib/redis
chmod 0755 /etc/redis

# Clean up.
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
