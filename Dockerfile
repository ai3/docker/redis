FROM debian:stable-slim

COPY build.sh /tmp/build.sh

RUN /tmp/build.sh && rm /tmp/build.sh

ENTRYPOINT ["/usr/bin/redis-server", "/etc/redis/redis.conf"]

